﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour
{
    public GameObject explodePrefab;
    public AudioClip explodeAudio;


    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector2 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
            if (touchPosition.x > 0 && transform.position.x <= 2.4f)
            {
                transform.Translate(.1f, 0, 0);
            }
            else if (touchPosition.x < 0 && transform.position.x >= -2.3f)
            {
                transform.Translate(-.1f, 0, 0);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("enemy"))
        {
            Instantiate(explodePrefab, transform.position, transform.rotation);  //Particles effect
            Destroy(GameObject.FindWithTag("enemy"));                            // Destroying waterdrop
            AudioSource.PlayClipAtPoint(explodeAudio,transform.position);        // playing explode audio
            Destroy(gameObject);                                                 // Destroy player/Bird
            FindObjectOfType<GameManager>().GameOver();                          // Accessing GameOver function from GameManager Script
        }
    }
}