﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMe : MonoBehaviour
{

    private void Start()
    {
        Destroy(gameObject, 2f);   //Destroy gameobject with delay of 2 second
    }
}
