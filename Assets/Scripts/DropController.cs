﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropController : MonoBehaviour
{
    public GameObject shockPrefab;
    public AudioClip sparkAudio;


    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Wire"))
        {
            Instantiate(shockPrefab, transform.position, Quaternion.identity);  //particle effect
            AudioSource.PlayClipAtPoint(sparkAudio, transform.position);        //playing audio at point
            Destroy(gameObject);                                                //Destroying waterdrop
        }
    }
}