﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaterDropSpawner : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject waterPrefab;
    public float timeToSpawn = 2f;
    public float timeBetweenWaves = 1f;
    public static int ScoreCount = 1;
    public Text ScoreText;

    private void Start()
    {
        InvokeRepeating("LevelUp", 5f, 10f);
    }

    private void Update()
    {
        if (Time.time > timeToSpawn)
        {
            SpawnWaterDrop();
            timeToSpawn = Time.time + timeBetweenWaves;
            ScoreText.text = "" + ScoreCount;
        }
    }

    void SpawnWaterDrop()
    {
        int randomIndex = Random.Range(0, spawnPoints.Length);

        for (int i = 0; i < spawnPoints.Length; i++)
        {
            if (randomIndex == i)
            {
                Instantiate(waterPrefab, spawnPoints[i].position,
                    Quaternion.identity);  //Instantiate waterdrop in random position using random numbers
            }
        }
    }

    public void LevelUp()
    {
        float newSpeed = timeBetweenWaves - .01f;
        timeBetweenWaves = newSpeed;
        ScoreCount += 1;  //Adding score by 1
    }
}