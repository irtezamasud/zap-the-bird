﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public float restartDelay = 1f;
    
    public GameObject bird;
    public GameObject pauseMenuUI;
    public GameObject ScoreText;
    
    public void GameOver()
    {
        Invoke("RestartGame", restartDelay);
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    public void Resume()
    {
        bird.SetActive(true);
        ScoreText.SetActive(true);
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        
        
    }

    public void Pause()
    {
        bird.SetActive(false);
        ScoreText.SetActive(false);
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        
    }

    public void MainLevel()
    {
        SceneManager.LoadScene(1);
    }
}
